package fr.formation.correction.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@SequenceGenerator(name = "filiere_gen", sequenceName = "filiere_seq", initialValue = 100, allocationSize = 1)
public class Filiere {

	@Id
	@GeneratedValue(generator = "filiere_gen")
	private Integer id;

	private String libelle;

	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	private List<Module> modules;

	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	private List<Personne> stagiaires;

	public Filiere() {

	}

	public Filiere(Integer id, String libelle, List<Module> modules, List<Personne> stagiaire) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.modules = modules;
		this.stagiaires = stagiaire;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Personne> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Personne> stagiaire) {
		this.stagiaires = stagiaire;
	}

}
