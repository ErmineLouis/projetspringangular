package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Module;
import fr.formation.correction.service.ModuleService;

@CrossOrigin
@RestController
@RequestMapping("/api/module")
public class ModuleController {

	@Autowired
	private ModuleService ms;

	@GetMapping("")
	public List<Module> findAll() {
		return ms.findAll();
	}

	@GetMapping("/{id}")
	public Module getById(@PathVariable Integer id) {
		return ms.getById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Le module n'existe pas"));
	}

	@PostMapping("")
	public void createModule(@RequestBody Module p) {
		ms.create(p);
	}

	@PutMapping("")
	public void updateModule(@RequestBody Module p) {
		ms.update(p);
	}

	@PutMapping("/{id}")
	public void deleteModule(@PathVariable Integer id) {
		ms.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Le module n'existe pas"));
	}

	/*
	 * Renvoie les modules d'une filière
	 */
	@GetMapping("/filiere/{id}")
	public List<Module> getModulesOfFiliere(@PathVariable Integer id) {
		return ms.findByFiliereId(id);
	}

	/*
	 * Renvoie les modules qui n'ont pas de formateur assignés
	 */
	@GetMapping("/formateurMissing")
	public List<Module> getModuleWithoutFormateur() {
		return ms.getModuleWithoutFormateur();
	}
}
