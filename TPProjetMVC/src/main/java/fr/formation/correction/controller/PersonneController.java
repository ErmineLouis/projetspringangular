package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Personne;
import fr.formation.correction.service.PersonneService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/personne")
public class PersonneController {

	@Autowired
	private PersonneService ps;

	@GetMapping("")
	public List<Personne> findAll() {
		return ps.findAll();
	}

	@GetMapping("/{id}")
	public Personne getById(@PathVariable Integer id) {
		return ps.getById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La personne n'existe pas"));
	}

	@PostMapping("")
	public void createPersonne(@RequestBody Personne p) {
		ps.create(p);
	}

	@PutMapping("")
	public void updatePersonne(@RequestBody Personne p) {
		ps.update(p);
	}

	@PutMapping("/{id}")
	public void deletePersonne(@PathVariable Integer id) {
		ps.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La personne n'existe pas"));
	}

	/*
	 * Renvoie la liste des formateurs intervenant sur une filière donné en paramètre
	 */
	@GetMapping("/formateurs")
	public List<Personne> getFormateursOfFiliere(@RequestParam(name = "filiere_id") Integer id) {
		Filiere f = new Filiere();
		f.setId(id);
		return ps.getFormateursOfFiliere(f);
	}
}
