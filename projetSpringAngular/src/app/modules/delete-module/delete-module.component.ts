import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModuleService } from '../module.service';

@Component({
  selector: 'delete-module',
  templateUrl: './delete-module.component.html',
  styleUrls: ['./delete-module.component.css']
})
export class DeleteModuleComponent implements OnInit {
  @Output() postEvent: EventEmitter<string> = new EventEmitter();

  identifiant:number=0

  constructor(private moduleService : ModuleService) { }

  ngOnInit(): void {
  }

  handleSubmit() {

    this.moduleService.deleteModule(this.identifiant).subscribe((res) => {

      this.postEvent.emit("Suppression du module " + this.identifiant)
    })
  
    

    
  }

}
