import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleComponent } from './module/module.component';
import { HttpClientModule } from '@angular/common/http';
import { AddModuleComponent } from './add-module/add-module.component';
import { ListModuleComponent } from './list-module/list-module.component';
import { FormsModule } from '@angular/forms';
import { DeleteModuleComponent } from './delete-module/delete-module.component';
import { ItemModuleComponent } from './item-module/item-module.component';
import { RouterModule } from '@angular/router';
import { DoneComponent } from './done/done.component';


@NgModule({
  declarations: [
    ModuleComponent,
    AddModuleComponent,
    ListModuleComponent,
    DeleteModuleComponent,
    ItemModuleComponent,
    DoneComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([

      {path:"itemModule/:id", component:ItemModuleComponent},
      {path:"done", component: DoneComponent}
    ])

    
  ],
  exports: [
    ModuleComponent
 
  ]
})
export class ModulesModule { }
