import { Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Subject } from 'rxjs';
import { Module } from 'src/app/interfaces/module.interface';

import { ModuleService } from '../module.service';

@Component({
  selector: 'list-module',
  templateUrl: './list-module.component.html',
  styleUrls: ['./list-module.component.css']
})
export class ListModuleComponent implements OnInit {
  

  ModuleList:Module[]=[]
  @Input() update: Subject<boolean> = new Subject();

  constructor(private moduleService : ModuleService) { 

    this.moduleService.getModules().subscribe(value => {
      this.ModuleList = value
    })
  
  }

  ngOnInit(): void {

    this.update.subscribe(v => { 
      this.updateList();
    });
  }

  updateList(){
    this.moduleService.getModules().subscribe(res => {
      this.ModuleList = res
    });
  }

}
