import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs";
import { Module } from "src/app/interfaces/module.interface";

const API : string = "http://localhost:8082/api/module"


@Injectable({
    providedIn: 'root'
  })


export class ModuleService {

  postId:number=0


    constructor(private http:HttpClient) { }


     getModules():Observable<Module[]>{


        return this.http.get<Module[]>(API)
      }

      postModule(module :  Module):Observable<HttpResponse<Module>>{

        const headers = { 'content-type': 'application/json'}
        return this.http.post<Module>(API, module, {headers, observe:'response'})
    }

    getModule(id:string):Observable<Module>{
      return this.http.get<Module>(API+'/'+id);
    }

    deleteModule(id :number):Observable<Module>  {
      const body = {suppression:id}
      return this.http.put<Module>(API+"/"+id,body);
    }

    updateModule(module:Module){
      return this.http.put(API, module);
    }


    
  }