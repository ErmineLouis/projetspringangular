import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Module } from 'src/app/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {

  listNeedUpdate: Subject<boolean> = new Subject();

  ngOnInit(): void {
     
  }

  updateList() {

    this.listNeedUpdate.next(true)
  }

 

}
