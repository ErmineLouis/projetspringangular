import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.css']
})
export class DoneComponent implements OnInit {
  @Output() doneEvent: EventEmitter<string> = new EventEmitter();

  constructor() {
    this.doneEvent.emit("done")
   }


  ngOnInit(): void {
  }

}
