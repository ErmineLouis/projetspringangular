import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Module } from 'src/app/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'add-module',
  templateUrl: './add-module.component.html',
  styleUrls: ['./add-module.component.css']
})
export class AddModuleComponent implements OnInit {


  @Output() postEvent: EventEmitter<string> = new EventEmitter();
  module:Module={
    
    id:0,

    dateDebut:"",
    dateFin:"",
    libelle:"",
    filiere:undefined,
    formateur:undefined
  }

  

  constructor(private moduleService : ModuleService) { 

    
  }

  ngOnInit(): void {
  }

  handleSubmit() {

    this.moduleService.postModule(this.module) .subscribe((res) => {
      console.log(res);
      this.postEvent.emit("Enregistrement d'un module: " + this.module)
    })
   
  }

}
