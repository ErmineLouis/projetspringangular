import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemModuleComponent } from './item-module.component';

describe('ItemModuleComponent', () => {
  let component: ItemModuleComponent;
  let fixture: ComponentFixture<ItemModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
