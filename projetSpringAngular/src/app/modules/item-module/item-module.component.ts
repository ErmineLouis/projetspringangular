import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Module } from 'src/app/interfaces/module.interface';
import { ModuleService } from '../module.service';

@Component({
  selector: 'item-module',
  templateUrl: './item-module.component.html',
  styleUrls: ['./item-module.component.css']
})
export class ItemModuleComponent implements OnInit {

  urlParam: string | null = '';

  module: Module = {
    libelle: "",
    id: 0,
    dateDebut: '',
    dateFin: ''
  }

  constructor(private route: ActivatedRoute, private moduleService: ModuleService, private homeRoute : Router) { }

  ngOnInit(): void {

    this.route.
    paramMap.subscribe(params => {
      this.urlParam = params.get('id');
      if(this.urlParam != null){
        this.moduleService.getModule(this.urlParam).subscribe((module) => {
          this.module=module;
        });
      }
    }
    );
  }

  handleUpdate(){

    this.moduleService
        .updateModule(this.module)
        .subscribe((res) => {
        })


    //TODO : redirect to home
    this.homeRoute.navigate(["/done"]);
  }


}
