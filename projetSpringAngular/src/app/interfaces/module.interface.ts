

export interface Module{

    id:number,

    dateDebut:string,
    dateFin:string,
    libelle:string,
    filiere?:number,
    formateur?:number
}