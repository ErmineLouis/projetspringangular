import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemFiliereComponent } from './item-filiere.component';

describe('ItemFiliereComponent', () => {
  let component: ItemFiliereComponent;
  let fixture: ComponentFixture<ItemFiliereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemFiliereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
