import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Filiere } from '../filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'item-filiere',
  templateUrl: './item-filiere.component.html',
  styleUrls: ['./item-filiere.component.css']
})
export class ItemFiliereComponent implements OnInit {
  urlParam: string | null = '';

  filiere: Filiere = {
    libelle : "daz",
    modules : [],
    stagiaires : []
  }

  constructor(private route: ActivatedRoute, private filiereService: FiliereService, private homeRoute : Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.urlParam = params.get('id');
      if(this.urlParam != null){
        this.filiereService.getFiliere(this.urlParam).subscribe((filiere) => {
          this.filiere = filiere;
        });
      }
    }
    );


  }

  handleUpdate(event:any){
    if(event.submitter.value == "update"){
      console.log(this.filiere)
      this.filiereService
        .updateFiliere({
          id : this.filiere.id,
          libelle : this.filiere.libelle
        })
        .subscribe((res) => {
        })
    }
    else if (event.submitter.value == "delete"){
      if(this.filiere.id != undefined)
        this.filiereService
          .deleteFiliere(this.filiere.id)
          .subscribe((f:Filiere) => {});
    }

    //TODO : redirect to home
    this.homeRoute.navigateByUrl("");
  }

}
