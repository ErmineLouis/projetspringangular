import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Filiere } from '../filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'list-filiere',
  templateUrl: './list-filiere.component.html',
  styleUrls: ['./list-filiere.component.css']
})
export class ListFiliereComponent implements OnInit {

  filieres: Filiere[] = [];
  @Input() update: Subject<boolean> = new Subject();

  constructor(private filiereService: FiliereService) { 
    this.updateList();
  }

  ngOnInit(): void {
    this.update.subscribe(v => { 
      this.updateList();
    });
  }

  updateList(){
    this.filiereService.getFilieres().subscribe(res => {
      this.filieres = res
    });
  }

}
