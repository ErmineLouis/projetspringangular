import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Filiere } from '../filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'form-filiere',
  templateUrl: './form-filiere.component.html',
  styleUrls: ['./form-filiere.component.css']
})
export class FormFiliereComponent implements OnInit {

  filiere: Filiere = {
    libelle : "",
    modules : [],
    stagiaires : []
  }

  @Output() postEvent: EventEmitter<string> = new EventEmitter();

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.filiereService
      .postFiliere(this.filiere)
      .subscribe((res) => {
        console.log(res);
        this.postEvent.emit("Enregistrement d'une filiere : " + this.filiere)
      })
  }

}
