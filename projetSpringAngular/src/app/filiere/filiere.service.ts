import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Filiere } from './filiere.interface';
import { Observable } from 'rxjs';

// API des filieres
const API: string = "http://localhost:8082/api/filiere";

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http: HttpClient) { }

  getFilieres():Observable<Filiere[]>{
    return this.http.get<Filiere[]>(API);
  }
  getFiliere(id:string):Observable<Filiere>{
    return this.http.get<Filiere>(API+'/'+id);
  }

  postFiliere(filiere: Filiere):Observable<Filiere>{
    const headers = { 'content-type': 'application/json'}
    return this.http.post<Filiere>(API, filiere, {headers});
  }

  updateFiliere(filiere:any){
    return this.http.put(API, filiere);
  }

  deleteFiliere(id:number){
    return this.http.delete<Filiere>(API+'/'+id);
  }
}
