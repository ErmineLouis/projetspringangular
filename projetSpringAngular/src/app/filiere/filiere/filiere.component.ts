import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ListFiliereComponent } from '../list-filiere/list-filiere.component';

@Component({
  selector: 'filiere',
  templateUrl: './filiere.component.html',
  styleUrls: ['./filiere.component.css']
})
export class FiliereComponent implements OnInit {

  listNeedUpdate: Subject<boolean> = new Subject();

  constructor() { }

  ngOnInit(): void {
  }

  updateList(){
    this.listNeedUpdate.next(true);
  }

}
