export interface Filiere {
  id?: number; // champ optionnel
  libelle: string;
  modules: any[];
  stagiaires: any[];
}