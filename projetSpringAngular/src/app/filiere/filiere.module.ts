import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ListFiliereComponent } from './list-filiere/list-filiere.component';
import { FormFiliereComponent } from './form-filiere/form-filiere.component';
import { FiliereComponent } from './filiere/filiere.component';
import { ItemFiliereComponent } from './item-filiere/item-filiere.component';



@NgModule({
  declarations: [
    ListFiliereComponent,
    FormFiliereComponent,
    FiliereComponent,
    ItemFiliereComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: "itemFiliere/:id", component: ItemFiliereComponent}
    ])
  ],
  exports:[
    FiliereComponent
  ]
})
export class FiliereModule { }
